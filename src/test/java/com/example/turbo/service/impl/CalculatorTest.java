package com.example.turbo.service.impl;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
private CalculatorImpl calculator;
    @BeforeEach
    void setUp() {
        calculator=new CalculatorImpl();

    }

    @AfterEach
    void tearDown() {
    calculator=null;
    }

    @Test
    void sum() {
    assertEquals(7,calculator.sum(4,3));
    }

    @Test
    void hello() {
   assertThat(calculator.hello().equals("hello")).isEqualTo(true);
    }
}