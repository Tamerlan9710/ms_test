package com.example.turbo.mapper;

import com.example.turbo.dto.CarDto;
import com.example.turbo.model.Car;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;


@Mapper (componentModel = "Spring",unmappedTargetPolicy = IGNORE)

public interface CarMapper {
    Car carDtoToCar(CarDto carDto);
    CarDto carToCarDto(Car car);


    List<Car>carDtoListToCar(List<CarDto>carDtoList);
    List<CarDto>carListToCarDto(List<Car>carList);


}
