package com.example.turbo.repository;

import com.example.turbo.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CarRepository extends JpaRepository<Car,Long> {
    Optional<Car>findById(Long id);
    Optional<Car>findByBrandAndCity(String brand,String city);

    @Override
    List<Car> findAll();
}
