package com.example.turbo.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@ToString
@Data
@NoArgsConstructor
public class CarDto {
    String brand;
    String model;
    String city;

}
