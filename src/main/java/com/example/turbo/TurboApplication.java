package com.example.turbo;


import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurboApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(TurboApplication.class,args);
    }


    @Override
    public void run(String... args) throws Exception {
    }
}


















