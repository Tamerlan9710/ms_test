package com.example.turbo.controller;

import com.example.turbo.dto.CarDto;
import com.example.turbo.model.Car;
import com.example.turbo.service.impl.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/car/")
@RequiredArgsConstructor
public class CarController {
    private final CarService service;

    @GetMapping("/id/{id}")
    public Car getCar(@PathVariable(name = "id") Long id) {
        return service.getCar(id);
    }

    @PostMapping()
    public CarDto saveCar(@RequestBody CarDto carDto) {
        return service.saveCar(carDto);
    }

    @GetMapping("/find-all")
    public List<CarDto> findAll() {
        return service.findAll();
    }

    @GetMapping
    public Car findBrandAndCity
            (@Param("brand") String brand,
             @RequestParam(name = "city", required = false) String city) {
      return service.findBrandAndCity(brand, city);
    }
    @DeleteMapping ("/id/{id}")
    public String deleteCarDto(@PathVariable(name = "id") Long id){
       return service.deleteCarDto(id);
    }
    @PutMapping("/update/id/{id}")
    public  CarDto update(@PathVariable(name = "id") Long id, @RequestBody CarDto carDto){
       return service.update(id, carDto);
    }
}