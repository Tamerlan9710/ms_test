package com.example.turbo.service.impl;

import com.example.turbo.dto.CarDto;
import com.example.turbo.mapper.CarMapper;
import com.example.turbo.model.Car;
import com.example.turbo.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;
    private final CarMapper mapper;

    @Override
    public Car getCar(Long id) {
        Optional<Car> car = carRepository.findById(id);
        return car.orElse(new Car());
    }

    @Override
    public CarDto saveCar(CarDto carDto) {
        Car car = mapper.carDtoToCar(carDto);
        return mapper.carToCarDto(carRepository.save(car));
    }

    @Override
    public List<CarDto> findAll() {
        List<Car> cars = carRepository.findAll();
        return mapper.carListToCarDto(cars);
    }

    @Override
    public Car findBrandAndCity(String brand, String city) {
        Optional<Car> car = carRepository.findByBrandAndCity(brand, city);
        return car.orElse(new Car());
    }

    @Override
    public String deleteCarDto(Long id) {
        String message;
        Optional<Car> car = carRepository.findById(id);
        if (car.isPresent()) {
            carRepository.deleteById(id);
            message = "Was deleted car by id equals to";

        } else {
            message = "User not found by id equals to";
        }
        return message + id;
    }

    @Override
    public CarDto update(Long id, CarDto carDto) {
        Optional<Car> car = carRepository.findById(id);
        if (car.isPresent()) {
            car.get().setCity(carDto.getCity());
            car.get().setModel(carDto.getModel());
            car.get().setBrand(carDto.getBrand());
            carRepository.save(car.get());
        }
        return mapper.carToCarDto(car.get());
    }

}







