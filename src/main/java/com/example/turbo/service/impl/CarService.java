package com.example.turbo.service.impl;

import com.example.turbo.dto.CarDto;
import com.example.turbo.model.Car;

import java.util.List;


public interface CarService {

    Car getCar(Long id);

    CarDto saveCar(CarDto carDto);

    List<CarDto> findAll();

    Car findBrandAndCity(String brand, String city);
    String deleteCarDto(Long id);
    CarDto update(Long id, CarDto carDto);

}
